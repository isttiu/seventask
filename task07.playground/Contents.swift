import UIKit

//первое задание
struct Carpas{

	var model: String
	var yearIssue: Int
	var trunk: Int
	var engine: Bool
	var window: Bool
	var fulltrunk: Bool

	enum action{

		case engineOn
		case engineOff
		case windowOpen
		case windowClose
		case cargoLoad
		case cargoUnLoad

	}

	func actiontime(act: action){

		switch act{

		case .engineOn: engine == true
			print("Дивгатель \(model) запущен")
		case .engineOff: engine == false
			print("Дивгатель \(model) заглушен")
		case .windowOpen: window == true
			print("Окна \(model) открыты")
		case .windowClose: window == false
			print("Окна \(model) закрыты")
		case .cargoLoad: fulltrunk == true
			print("Багаж \(model) загружается")
		case .cargoUnLoad: fulltrunk == false
			print("Багаж \(model) выгружается")
		}

	}
}

var c = Carpas(model: "Audi", yearIssue: 2021, trunk: 400,engine: true,window: true,fulltrunk: false )
c.actiontime(act: .windowOpen)
c.actiontime(act: .cargoUnLoad)
print("")
//словарь, без понятия как из структур сделать словарь
let pascar = ["model", "yearIssue", "trunk", "engine", "window", "fulltrunk"]
let ac = ["Audi", "2021", "On", "Off","On","Off"]
var seq = zip(pascar, ac)

var dict = Dictionary(seq, uniquingKeysWith: {return $1})
for (key,value) in dict{
	print("\(key): \(value)")
}
struct Truck{

	var model: String
	var yearIssue: Int
	var trunk: Int
	var engineOn: Bool
	var windowOpen: Bool
	var fulltrunk: Bool


}
//код с картинки №1
//были две сильные ссылки
class Car{
	weak var driver: Man?

	deinit{
		print("машина удалена из памяти")
	}
}

class Man{
	var myCar: Car?

	deinit{
		print("мужчина удален из памяти")
	}
}

var car: Car? = Car()
var man: Man? = Man()

car?.driver = man
man?.myCar = car

car = nil
man = nil
print("")
//код с картинки №2

class Mman{
	var pasport: Passport?
	deinit{
		print("мужчина удален из памяти")
	}
}
class Passport{
	unowned let mman: Mman
	init(mman: Mman){
		self.mman = mman
	}
	deinit{
		print("паспорт удален из памяти")
	}
}
var mman: Mman? = Mman()
var passport: Passport? = Passport(mman: mman!)
mman?.pasport = passport
passport = nil
mman = nil
